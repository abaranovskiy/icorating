import React from 'react';
import { StyleSheet } from 'react-native';
import { StackNavigator } from 'react-navigation';
import SplashScene from './src/scenes/splashScene';
import LoginScene from './src/scenes/loginScene';
import FondScene from './src/scenes/fondScene';
import CurrencyScene from './src/scenes/currencyScene';
import PortfolioScene from './src/scenes/portfolioScene';

export default StackNavigator({
    Splash: {screen: SplashScene},
    Login: {screen: LoginScene},
    Fond: {screen: FondScene},
    Currency: {screen: CurrencyScene},
    Portfolio: {screen: PortfolioScene}
}, {
    cardStyle: { backgroundColor: 'rgba(28,28,28,1)' }
});