import React, { Component } from 'react';
import { StatusBar, View, AsyncStorage } from 'react-native';
import {
    Header,
    Container,
    Content,
    Spinner,
    Footer,
    FooterTab,
    Button,
    Title,
    Body,
    Right,
    Left,
    Text
} from 'native-base';

import Moment from 'moment';
import Icon from 'react-native-vector-icons/FontAwesome';

import Settings from '../../settings';
const settings = new Settings();

import PieChart from '../components/Fond/PieChart';
import Tokens from '../components/Fond/Tokens';

export default class FondScene extends Component {
    static navigationOptions = ({ navigation }) => ({
       header: null
    });

    constructor(props) {
        super(props);

        this.state = {
            loading: true
        };
    }

    componentDidMount() {
        AsyncStorage.getItem('userId', (err, userId) => {
            if (userId == null) {
                this.props.navigation.navigate('Login', { passProps: this.props.navigation.state.params });
                return ;
            }
            
            fetch(`${settings.apiUrl}/user/data`, {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + userId
                }
            })
            .then(response => {
                if (response.status === 401) {
                    this.props.navigation.navigate('Login', { passProps: this.props.navigation.state.params });
                    throw new Error('');
                }

                return response.json();
            })
            .then(resultJson => {
                this.setState(Object.assign({}, resultJson.fondData, {loading: false}));
            }).catch(err => {
                console.log(err);
            });
        });
    }

    render() {

        if (this.state.loading) {
            return (
                <Container>
                    <StatusBar hidden={true} />
                    <Content contentContainerStyle={{ flex: 1, justifyContent: 'center' }}>
                        <Spinner color="#fff" />
                    </Content>
                </Container>
            );
        }

        return (
            <Container style={{flex: 1}}>
                <StatusBar hidden={true} />
                <Header style={{backgroundColor: 'rgba(21,21,21,1)', borderBottomColor: '#191919', paddingTop: 0, height: 40}}>
                    <Left />
                    <Body>
                        <Title style={{color: '#fff'}}>Хэдж. фонд</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => {
                            AsyncStorage.removeItem('userId');
                            this.props.navigation.navigate('Login', { passProps: this.props.navigation.state.params });
                        }}>
                            <Text style={{color: '#fff'}}>Выход</Text>
                        </Button>
                    </Right>
                </Header>
                <Content style={{backgroundColor: '#000'}}>
                    <View style={{justifyContent: 'center', backgroundColor: '#000', paddingVertical: 6, marginTop: 10}}>
                        <Text style={{color: '#fff', alignSelf: 'center', fontSize: 14}}>{this.getDate()}</Text>
                    </View>
                    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 18}}>
                        <PieChart
                            portfolioValue={this.portfolioValue()}
                            profitValue={this.profitValue()}
                            investedValue={this.investedValue()}
                            tokens={this.latestTokens()}
                        />
                    </View>
                    <View style={{flex: 1, marginTop: 20}}>
                        <Tokens list={this.latestTokens()}/>
                    </View>
                </Content>
                <Footer style={{backgroundColor: 'rgba(21,21,21,1)', borderTopWidth: 0}}>
                    <FooterTab>
                        <Button active transparent>
                            <Icon name="pie-chart" style={{color: '#C38857', fontSize: 30}}/>
                        </Button>
                        <Button transparent onPress={() => {
                            this.props.navigation.navigate('Currency', { passProps: this.props.navigation.state.params });
                        }}>
                            <Icon name="dollar" style={{color: '#B8B8B8', fontSize: 30}}/>
                        </Button>
                        <Button transparent onPress={() => {
                            this.props.navigation.navigate('Portfolio', { passProps: this.props.navigation.state.params });
                        }}>
                            <Icon name="shopping-bag" style={{color: '#B8B8B8', fontSize: 30}}/>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }

    /**
     * Returns date of tokens
     * @returns {string}
     */
    getDate() {
        var first = this.state.latest_tokens[0];
        return Moment(first.date).format('DD.MM.YYYY');
    }

    /**
     * Returns portfolio value ("Стоимость портфеля")
     * @returns {number}
     * @private
     */
    portfolioValue() {
        return this.state.portfolio_value;
    }

    /**
     * Returns invented value ("Инвестированно")
     * @returns {number}
     * @private
     */
    investedValue() {
        return this.state.invested_value;
    }

    /**
     * Returns profit value ("Доходность")
     * @returns {string}
     * @private
     */
    profitValue() {
        var value = this.state.profit_value * 100;
        return value.toFixed(0) + '%';
    }
    
    /**
     * Returns latest tokens
     * @returns {Object}
     * @private
     */
    latestTokens() {
        return this.state.latest_tokens;
    }
}