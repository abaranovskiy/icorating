import React, { Component } from 'react';
import {
    ActivityIndicator,
    StatusBar,
    View,
    Image,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    AsyncStorage
} from 'react-native';
import Toast from 'react-native-easy-toast';

import Settings from '../../settings';
const settings = new Settings();

export default class LoginScene extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: null
    });

    constructor(props) {
        super(props);

        this.state = {
            loading: false
        }
    }

    render() {
        return (
            <KeyboardAvoidingView behavior="padding" style={{
                flex: 1,
                backgroundColor: 'rgba(0,0,0,1)'
            }}>
                <StatusBar
                    backgroundColor="#3F51B5"
                    barStyle="light-content"
                />
                <View style={{
                    flex: 2,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <Image
                        style={{
                            height: 100,
                            width: 100
                        }}
                        source={{uri: 'http://i.imgur.com/N8X8KzC.png'}}
                    />
                    <Text style={{
                        color: '#fff',
                        fontSize: 16,
                        lineHeight: 24,
                        width: '80%',
                        marginTop: 14,
                        textAlign: 'center'
                    }}>Чтобы войти укажите свою почту и пароль</Text>
                </View>
                <View style={{ flex: 1}}>
                    <TextInput style={{
                        height: 50,
                        backgroundColor: 'rgba(28,28,28,1)',
                        marginBottom: 2,
                        color: '#fff',
                        paddingHorizontal: 10
                    }}
                       placeholder="Почта"
                       placeholderTextColor="#B8B8B8"
                       returnKeyType="next"
                       keyboardType="email-address"
                       autoCapitalize="none"
                       autoCorrect={false}
                       onSubmitEditing={() => this.password.focus()}
                       onChangeText={(input) => this.setState({ email: input })}
                       value={this.state.email}
                    />
                    <TextInput style={{
                        height: 50,
                        backgroundColor: 'rgba(28,28,28,1)',
                        marginBottom: 2,
                        color: '#fff',
                        paddingHorizontal: 10
                    }}
                       placeholder="Пароль"
                       placeholderTextColor="#B8B8B8"
                       returnKeyType="go"
                       secureTextEntry
                       ref={(input) => this.password = input}
                       onChangeText={(input) => this.setState({ password: input })}
                       value={this.state.password}
                    />
                    <TouchableOpacity onPress={this.login.bind(this)} style={{
                         backgroundColor: 'rgba(42,42,42,1)',
                        borderRadius: 2,
                        paddingVertical: 15,
                        marginTop: 10
                    }} underlayColor='#99d9f4'>
                        <View>{this.loadingText()}</View>
                    </TouchableOpacity>
                </View>
                <Toast
                    ref="error"
                    style={{ backgroundColor: 'rgba(174,126,88,1)', flex: 1}}
                    position='top'
                    positionValue={20}
                    fadeInDuration={750}
                    fadeOutDuration={1000}
                    opacity={0.8}
                />
            </KeyboardAvoidingView>
        );
    }

    loadingText() {
        if (this.state.loading) {
            return <ActivityIndicator size='small' color='white' />
        }
        else {
            return (
                <Text style={{
                    textAlign: 'center',
                    color: '#fff',
                    fontWeight: '500'
                    }}>ВОЙТИ
                </Text>
            );
        }
    }

    login() {
        this.setState({ loading: true });

        fetch(`${settings.apiUrl}/auth/login`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: this.state.email,
                password: this.state.password
            })
        })
        .then(response => response.json())
        .then(json => {
            if (json.success !== true) {
                throw new Error('');
                return;
            }

            AsyncStorage.setItem('userId', encodeURIComponent(json.user_id));

            this.setState({ loading: false });
            this.props.navigation.navigate('Fond', { passProps: this.props.navigation.state.params })
        })
        .catch(err => {
            this.refs.error.show(err.message || 'Не верные данные. Попробуйте еще раз.', 4000);
            this.setState({ loading: false });
        });
    }
}

module.exports = LoginScene;