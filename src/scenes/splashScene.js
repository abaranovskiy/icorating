import React, { Component } from 'react'
import { Image, StatusBar, AsyncStorage } from 'react-native'
import { View, Spinner } from 'native-base'
import { NavigationActions } from 'react-navigation'

import Settings from '../../settings';
const settings = new Settings();

export default class SplashScene extends Component {
    static navigationOptions = ({ navigation }) => ({
        header: null
    });

    constructor(props) {
        super(props);

        this.state = {
            action: 'Login'
        };

        AsyncStorage.getItem('userId', (err, result) => {
            if (result != null) {
                this.setState({ action: 'Fond' })
            }
            this.forward();
        });
    }

    forward() {
        const resetAction = NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: this.state.action, returning: true })
            ]
        });
        setTimeout(() => {
            this.props.navigation.dispatch(resetAction)
        }, 2000);
    }

     render() {
         return (
             <View style={{
                 flex: 1,
                 backgroundColor: 'rgba(28,28,28,1)'
                }}>
                 <StatusBar
                     backgroundColor="#3F51B5"
                     barStyle="light-content"
                 />
                 <View style={{
                     flex: 2,
                     justifyContent: 'center',
                     alignItems: 'center'
                 }}>
                     <Image style={{
                         height: 100,
                         width: 100
                     }} source={{uri: 'http://i.imgur.com/N8X8KzC.png'}} />
                     <Spinner color='white' />
                </View>
         </View>
         )
     }
}