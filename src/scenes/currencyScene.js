import React, { Component } from 'react';
import { StatusBar, View, AsyncStorage } from 'react-native';
import {
    Container,
    Content,
    Spinner,
    Footer,
    FooterTab,
    Button,
    Text,
    Header,
    Body,
    Title,
    List,
    ListItem
} from 'native-base';

import Moment from 'moment';
import Icon from 'react-native-vector-icons/FontAwesome';

import Settings from '../../settings';
const settings = new Settings();

export default class CurrencyScene extends Component {
    static navigationOptions = ({ navigation }) => ({
       header: null
    });

    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            allPortfolio: [],
            portfolio: [],
            investor: {}
        };
    }

    componentDidMount() {
        AsyncStorage.getItem('userId', (err, userId) => {
            if (userId == null) {
                this.props.navigation.navigate('Login', { passProps: this.props.navigation.state.params });
                return ;
            }

            fetch(`${settings.apiUrl}/user/data`, {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + userId
                }
            })
            .then(response => {
                if (response.status === 401) {
                    this.props.navigation.navigate('Login', { passProps: this.props.navigation.state.params });
                    throw new Error('');
                }

                return response.json();
            })
            .then(resultJson => {
                this.setState(Object.assign({}, resultJson.fondData, {loading: false}));
            }).catch(err => {
                console.log(err);
            });
        });
    }

    render() {
        if (this.state.loading) {
            return (
                <Container>
                    <StatusBar hidden={true} />
                    <Content contentContainerStyle={{ flex: 1, justifyContent: 'center' }}>
                        <Spinner color="#fff" />
                    </Content>
                </Container>
            );
        }

        return (
            <Container>
                <StatusBar hidden={true} />
                <Header style={{backgroundColor: 'rgba(21,21,21,1)', borderBottomColor: '#191919', paddingTop: 0, height: 40}}>
                    <Body>
                        <Title style={{color: '#fff'}}>Валюты</Title>
                    </Body>
                </Header>
                <Content style={{backgroundColor: '#000'}}>
                    <View style={{justifyContent: 'center', backgroundColor: '#000', paddingVertical: 6, marginTop: 10}}>
                        <Text style={{color: '#fff', alignSelf: 'center', fontSize: 14}}>{this.getDate()}</Text>
                    </View>
                    <List
                        style={{marginTop: 12}}
                        dataArray={this.header()}
                        renderRow={(item) =>
                            <ListItem style={{marginLeft: 0, paddingLeft: 0, borderTopWidth: 1, borderBottomWidth: 0, borderColor: '#191919'}}>
                                <View style={{flex: 1, alignItems: 'center'}}>
                                    <Text style={{color: '#fff', alignSelf: 'center', fontSize: 12}}>{item.name}</Text>
                                </View>
                                <View style={{flex: 1}}>
                                    <Text style={{color: '#fff', alignSelf: 'center', fontSize: 12}}>{item.price}</Text>
                                </View>
                                <View style={{flex: 1, alignItems: 'flex-end'}}>
                                    <Text style={{color: '#B88254', alignSelf: 'center', fontSize: 12}}>{item.growth}</Text>
                                </View>
                            </ListItem>
                        }>
                    </List>
                    <List
                        dataArray={this.currencies()}
                        renderRow={(item) =>
                            <ListItem style={{marginLeft: 0, paddingLeft: 0, borderTopWidth: 1, borderBottomWidth: 0, borderColor: '#191919'}}>
                                <View style={{flex: 1, alignItems: 'center'}}>
                                    <Text style={{color: '#fff', alignSelf: 'center', fontSize: 15}}>{item.name}</Text>
                                </View>
                                <View style={{flex: 1}}>
                                    <Text style={{color: '#fff', alignSelf: 'center', fontSize: 15}}>${item.price.toPrecision(4)}</Text>
                                </View>
                                <View style={{flex: 1, alignItems: 'flex-end'}}>
                                    <Text style={{color: '#B88254', alignSelf: 'center', fontSize: 15}}>{item.growth.toFixed(2)}%</Text>
                                </View>
                            </ListItem>
                        }>
                    </List>
                </Content>

                <Footer style={{backgroundColor: 'rgba(21,21,21,1)', borderTopWidth: 0}}>
                    <FooterTab>
                        <Button transparent onPress={() => {
                                this.props.navigation.navigate('Fond', { passProps: this.props.navigation.state.params });
                            }}>
                            <Icon name="pie-chart" style={{color: '#B8B8B8', fontSize: 30}}/>
                        </Button>
                        <Button active transparent>
                            <Icon name="dollar" style={{color: '#C38857', fontSize: 30}}/>
                        </Button>
                        <Button transparent onPress={() => {
                            this.props.navigation.navigate('Portfolio', { passProps: this.props.navigation.state.params });
                        }}>
                            <Icon name="shopping-bag" style={{color: '#B8B8B8', fontSize: 30}}/>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }

    /**
     * Returns date of tokens
     * @returns {string}
     */
    getDate() {
        var first = this.state.latest_tokens[0];
        return Moment(first.date).format('DD.MM.YYYY');
    }

    header() {
        return [{
            name: 'Валюта',
            price: 'Цена',
            growth: 'Изменение'
        }];
    }

    /**
     * Returns currencies
     * @returns {Array}
     */
    currencies() {
        return this.state.latest_tokens.map(token => {
            return {
                name: token.name,
                price: token.price,
                growth: token.investor_change * 100
            }
        });
    }
}