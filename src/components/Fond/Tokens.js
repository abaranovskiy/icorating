import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { List, ListItem, Text } from 'native-base';

export default class Tokens extends Component {

    render() {
        return (
            <View>
                <List
                    dataArray={this.header()}
                    renderRow={(item) =>
                        <ListItem style={{marginLeft: 0, paddingLeft: 0, borderTopWidth: 1, borderBottomWidth: 0, borderColor: '#191919'}}>
                            <View style={{flex: 1, alignItems: 'center'}}>
                                <Text style={{color: '#fff', alignSelf: 'center', fontSize: 12}}>{item.name}</Text>
                            </View>
                            <View style={{flex: 1}}>
                                <Text style={{color: '#fff', alignSelf: 'center', fontSize: 12}}>{item.price}</Text>
                            </View>
                            <View style={{flex: 1, alignItems: 'flex-end'}}>
                                <Text style={{color: '#B88254', alignSelf: 'center', fontSize: 12}}>{item.fondShare}</Text>
                            </View>
                        </ListItem>
                    }>
                </List>
                <List
                    dataArray={this.currencies()}
                    renderRow={(item) =>
                        <ListItem style={{marginLeft: 0, paddingLeft: 0, borderTopWidth: 1, borderBottomWidth: 0, borderColor: '#191919'}}>
                            <View style={{flex: 1, alignItems: 'flex-start'}}>
                                <Text style={{color: '#fff', alignSelf: 'center', fontSize: 15}}>{this._getName(item)}</Text>
                            </View>
                            <View style={{flex: 1}}>
                                <Text style={{color: '#fff', alignSelf: 'center', fontSize: 15}}>{this._getPrice(item)}</Text>
                            </View>
                            <View style={{flex: 1, alignItems: 'flex-end'}}>
                                <Text style={{color: '#B88254', alignSelf: 'center', fontSize: 15}}>{this._getPortfolioShare(item)}</Text>
                            </View>
                        </ListItem>
                    }>
                </List>
            </View>
        );
    }

    header() {
        return [{
            name: 'Валюта',
            price: 'Цена',
            fondShare: 'Доля в портфеле'
        }];
    }

    currencies() {
        return this.props.list;
    }

    _getName(item) {
        return item.name;
    }

    _getPrice(item) {
        var price = parseFloat(item.price);
        return '$' + price.toPrecision(4);
    }

    _getPortfolioShare(item) {
        var fondShare = parseFloat(item.portfolio_share * 100);
        return fondShare.toFixed(2) + '%';
    }
}