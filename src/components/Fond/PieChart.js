import React, { Component } from 'react';
import { StyleSheet, View, Text} from 'react-native';
import Chart from 'react-native-pie-chart';

export default class PieChart extends Component {
    render() {
        return (
            <View>
                <Chart
                    chart_wh={230}
                    doughnut={true}
                    coverRadius={0.9}
                    coverFill={'#000'}
                    series={this.series()}
                    sliceColor={this.sliceColor()}
                />
                <View style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <View>
                        <Text
                            style={{ fontSize: 22, color: '#fff'}}>
                            $ {this.props.portfolioValue.toFixed(2)}
                        </Text>
                    </View>
                    <View>
                        <Text
                            style={{ fontSize: 12, color: '#fff' }}>
                            Стоимость портфеля
                        </Text>
                    </View>
                    <View style={{marginTop: 10}}>
                        <Text
                            style={{ fontSize: 12, color: '#C38857' }}>
                            Доходность: {this.props.profitValue}
                        </Text>
                    </View>
                    <View style={{marginTop: 10, alignItems: 'center'}}>
                        <View>
                            <Text
                                style={{ fontSize: 14, color: '#878787' }}>
                                Инвестированно:
                            </Text>
                        </View>
                        <View>
                            <Text
                                style={{ fontSize: 14, color: '#878787' }}>
                                $ {this.props.investedValue}
                            </Text>
                        </View>

                    </View>
                </View>
            </View>
        );
    }

    series() {
        return this.props.tokens.map(token => {
            return parseFloat(token.portfolio_share);
        });
    }

    sliceColor() {
        var colors = ['rgb(204,178,143)', 'rgb(128,111,89)', 'rgb(230,220,207)', 'rgb(191,150,96)', 'rgb(204,191,143)', 'rgb(128,119,89)', 'rgb(230,224,207)', 'rgb(191,170,96)', 'rgb(204,162,143)', 'rgb(128,101,89)', 'rgb(230,214,207)', 'rgb(191,125,96)'];

        return this.props.tokens.map(token => {
            colors.push(colors.shift());
            return colors[0];
        });
    }
}