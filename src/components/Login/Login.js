import React, { Component } from 'react';
import { StyleSheet, View, Image, Text, KeyboardAvoidingView, StatusBar } from 'react-native';
import LoginForm from './LoginForm';

export default class Login extends Component {
    render() {
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container} >
                <StatusBar
                    barStyle="light-content"
                />
                <View style={styles.logoContainer}>
                    <Image
                        style={styles.logo}
                        source={require('../../images/icorating.png')}
                    />

                    <Text style={styles.title}>Чтобы войти укажите свою почту и пароль</Text>
                </View>
                <View style={styles.formContainer}>
                    <LoginForm/>
                </View>
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,1)'
    },
    logoContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    logo: {
        width: 133,
        height: 133
    },
    title: {
        color: '#6A6A69',
        fontSize: 16,
        lineHeight: 24,
        width: '80%',
        marginTop: 14,
        textAlign: 'center'
    },
    formContainer: {
        flex: 1
    }
});