// =============================================================================
// POSTS API ROUTES ============================================================
// =============================================================================

/**
 * Expose the routes to our app with module.export
 * @param {express.Router} router - Инстанс express роутера
 * @param {Object} serviceLocator - модуль для работы с ресурсами
 * @return {express.Router}
 */
module.exports = function(router, serviceLocator) {
    var DataModule = serviceLocator.get('DataModule');

    // middleware to use for all requests
    router.use(function(req, res, next) {
        next(); // make sure we go to the next routes and don't stop here
    });

    // user login
    router.post('/login', function (req, res, next) {
        var email = req.body.email,
            password = req.body.password;

        // Аутентифицируем пользователя
        return DataModule.authenticate(email, password, function (err, userId, info) {
            if (err) return next(err);

            // Если такой юзер не найден, то выходим с ошибкой
            if (!userId) {
                return next(new Error(info.message));
            }

            // Успешно возвращаем пользователя
            return res.status(200).json({success: true, user_id: userId});
        });
    });
    return router;
};

