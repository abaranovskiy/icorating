// =============================================================================
// POSTS API ROUTES ============================================================
// =============================================================================

/**
 * Expose the routes to our app with module.export
 * @param {express.Router} router - Инстанс express роутера
 * @param {Object} serviceLocator - модуль для работы с ресурсами
 * @return {express.Router}
 */
module.exports = function(router, serviceLocator) {
    var DataModule = serviceLocator.get('DataModule');

    // middleware to use for all requests
    router.use(function(req, res, next) {
        next(); // make sure we go to the next routes and don't stop here
    });

    // clear cache -----------------------------------
    router.get('/flush', function (req, res, next) {
        return DataModule.flushCache(function (err) {
            return res.status(200).json({success: true});
        });
    });

    return router;
};