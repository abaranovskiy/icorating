// =============================================================================
// POSTS API ROUTES ============================================================
// =============================================================================

/**
 * Expose the routes to our app with module.export
 * @param {express.Router} router - Инстанс express роутера
 * @param {Object} serviceLocator - модуль для работы с ресурсами
 * @return {express.Router}
 */
module.exports = function(router, serviceLocator) {
    var RequireLoginMiddleware = serviceLocator.get('RequireLoginMiddleware'),
        DataModule = serviceLocator.get('DataModule');

    // middleware to use for all requests
    router.use(RequireLoginMiddleware, function(req, res, next) {
        next(); // make sure we go to the next routes and don't stop here
    });
    
    // get user  -------------------------------------
    router.get('/data', RequireLoginMiddleware, function (req, res, next) {
        var userId = req.user_id;

        return DataModule.getUserData(userId, function (err, userData) {
            if (err) return next(err);

            return res.status(200).json(userData);
        });
    });

    return router;
};