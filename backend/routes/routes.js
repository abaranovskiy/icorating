/**
 * Dependencies
 */
var express = require('express');

/**
 * Expose the routes to our app with module.export
 * @param {express} app - express application instance
 * @param {Object} serviceLocator - модуль для работы с ресурсами
 */
module.exports = function(app, serviceLocator) {

// =============================================================================
// SERVICE ROUTES ==============================================================
// =============================================================================

    // Ping service for monit
    // =========================

    app.route('/ping').get(function (req, res, next) {
        return res.status(200).end('OK');
    });

// =============================================================================
// DEVELOPMENT ROUTES ==========================================================
// =============================================================================

    //require('./dev')(app, serviceLocator);

// =============================================================================
// API ROUTER (Version 1) ======================================================
// =============================================================================

    // All of our routes will be prefixed with /api/v1/cache
    app.use('/api/v1/cache', require('./api/v1/cache')(express.Router(), serviceLocator));
    
    // All of our routes will be prefixed with /api/v1/auth
    app.use('/api/v1/auth', require('./api/v1/auth')(express.Router(), serviceLocator));

    // All of our routes will be prefixed with /api/v1/user
    app.use('/api/v1/user', require('./api/v1/user')(express.Router(), serviceLocator));

// =============================================================================
// ERROR HANDLE ROUTES =========================================================
// =============================================================================

    // NOT FOUND ERROR =========================

    app.use(function (req, res, next) {
        var err = new Error('Not found, url:' + req.url);
        err.status= 404;

        // Передаем ошибку финальному обработчику
        return next(err);
    });

    // ERROR HANDLERS =========================

    app.use(function (err, req, res, next) {
        // Set default server error
        res.status(err.status || (err.status = 500));

        // Set default error message
        err.message || (err.message = 'Internal Server Error');

        // Обрабатываем ошибку в зависимости от кода
        switch (err.status) {

            // Http 401 Unauthorized Error
            case 401:
                return res.json({error: 'Unauthorized'});

            // Http 404 Not Found Error
            case 404:
                return res.json({error: 'Not Found'});

            default: {
                return res.json({error: err.message});
            }
        }
    });
};
