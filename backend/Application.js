/**
 * Dependencies
 */
var express = require('express')
    , path = require('path')
    , request = require('request')
    , util = require('util')
    , bodyParser = require('body-parser')
    , EventEmitter = require('events').EventEmitter
;

module.exports = function Application (serviceLocator) {
    var configCommon = serviceLocator.get('configCommon');

    /**
     * Application class
     * @constructor
     */
    function Application () {
        EventEmitter.call(this);

        // Создаем Express приложение
        var app = this.express = express();

        // Отключаем раголовок X-Powered-By: Express
        app.disable('x-powered-by');

        // В production мы находимся за nginx
        app.enable('trust proxy');

        // Подкобчаем логирование только для development и staging
        if (process.env.NODE_ENV !== 'production') {
            app.use(require('morgan')('dev'));
        }

        // for parsing application/json
        app.use(bodyParser.json());
        // for parsing application/x-www-form-urlencoded
        app.use(bodyParser.urlencoded({ extended: true }));

        // REGISTER OUR ROUTES -------------------------------
        // =============================================================================
        require('./routes/routes')(app, serviceLocator);
    }

    // Inherit Application from EventEmitter
    util.inherits(Application, EventEmitter);

    /**
     * Application start
     * @returns {Application}
     */
    Application.prototype.start = function () {

        // Create application and listen to connections
        require('http').createServer(this.express).listen(configCommon.port, configCommon.host, function () {
            console.log("Application listening in %s on %s:%d with pid %d", process.env.NODE_ENV, this.address().address, this.address().port, process.pid);
        });

        return this;
    };

    /**
     * Graceful storing of application
     */
    Application.prototype.gracefulExit = function (err) {
        process.exit(1);
    };

    return Application;
};
