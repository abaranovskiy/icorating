/**
 * Dependencies
 */

/**
 * Middleware для гарантирования авторизованности пользователя
 * @param serviceLocator
 * @constructor
 */
module.exports = function RequireLoginMiddleware (serviceLocator) {

    return function RequireLoginMiddleware(req, res, next) {
        if (!req.headers.authorization) {
            var err = new Error('Access allowed only for registered users');
            err.status = 401;

            return next(err);
        }

        // Save user id in request
        req.user_id = decodeURIComponent(req.headers.authorization.split(' ')[1]);

        return next();
    };
};