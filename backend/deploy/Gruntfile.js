module.exports = function(grunt) {

    // Подгружаем все таски
    require('load-grunt-tasks')(grunt);

    var path                = require('path')
        , projectRelative   = '../';
    
    // Project configuration.
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        vars: {
            project_dir             : projectRelative,
            rev_dir                 : 'rev',
            deploy_dir              : '/var/www/.tmp',
            production_ip           : '89.221.211.173'
        },

        rsync: {
            options: {
                args: ["--verbose"],
                recursive: true,
                syncDest: true
            },

            revision: {
                options: {
                    args: [
                        "--verbose",
                        "--exclude=node_modules/*",
                        "--exclude=.git*",
                        "--exclude=.idea/",
                        "--exclude=.tmp/",
                        "--exclude=.DS_Store",
                        "--exclude=rev",
                        "--exclude=deploy"
                    ],
                    src: '<%= vars.project_dir %>',
                    dest: '<%= vars.rev_dir %>'
                }
            },

            'deploy-production': {
                options: {
                    src: ['<%= rsync.revision.options.dest %>/', 'run_script.sh'],
                    dest: '<%= vars.deploy_dir %>',
                    host: "root@<%= vars.production_ip %>",
                    syncDestIgnoreExcl: true
                }
            }
        },

        // Запуск скрипта разворачивания приложения
        shell: {

            // Запуск deployment скрипта в production сервер
            run_production_script: {
                command: "ssh root@<%= vars.production_ip %> 'sh <%= vars.deploy_dir %>/run_script.sh'",
                options: {
                    stdout: true,
                    stderr: true,
                    failOnError: true
                }
            }
        }
    });

    // Default task(s).
    grunt.registerTask('default', ['rsync:revision']);

    // Deploy production task.
    grunt.registerTask('deploy:production', [
        'default',
        'rsync:deploy-production',
        'shell:run_production_script'
    ]);
};