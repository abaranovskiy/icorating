#!/bin/bash -e

# Deployment script that do release new application code.

error_exit() {
    d=$(date '+%D %T :: ')
    echo "$d Error: $*" >&2
    rm -rf "${temp_dir}"
    exit 1
}

echo "Deploying icorating api ..."

timestamp=$(date +%s)

# Prepare constants
temp_dir="/var/www/.tmp"
versions_dir="/var/www/.versions"
application_dir="/var/www/icorating"
old_version_dir=$(readlink -f $application_dir)
new_version_dir="$versions_dir/$timestamp"

# Creating folders
mkdir -p "${versions_dir}" || error_exit "Could not create $versions_dir directory"

# Changing to $temp_dir directory
echo "Changing $temp_dir directory"
cd "$temp_dir" || error_exit "Could not change $temp_dir directory"
echo "Success!"

# Installing npm dependencies
echo "Installing npm dependencies"
npm install --production || error_exit "Could not install all npm dependencies"
echo "Success!"

# Move the new version into the $new_version_dir folder
echo "Move the nr   ew version into the $new_version_dir folder"
cd ".." && mv "${temp_dir}" "${new_version_dir}" || error_exit "Could not move new version to ${new_version_dir} directory"
echo "Success!"

# Point symlink to the new version
rm -f "${application_dir}" && ln -sv "${new_version_dir}" "${application_dir}" || error_exit "Could not create symlink to ${new_version_dir}"

# Reload pm2
echo "Reload pm2"
pm2 reload all || error_exit "Unable to reload pm2"
echo "Success!"

# Remove one older revision
cd "${versions_dir}" && ls -lt | tail -1 | awk '{print $NF}' | xargs rm -rf || echo "Unable to remove one older revision" >&2

echo "Deployment icorating api Done!"
exit 0