/**
 * Dependencies
 */
var request = require('request');
var NodeCache = require('node-cache');

/**
 * Название ключа для кэша
 * @type {string}
 */
var CACHE_KEY_PREFIX = 'user_data_';

// Инициализируем модуль для кэша
var cache = new NodeCache({ stdTTL: 3600 });

// Инвесторы
var investors = [
    {id: 'ФГ', email: 'i1', password: 'i1'},
    {id: 'ФД', email: 'i2', password: 'i2'},
    {id: 'КС', email: 'i3', password: 'i3'},
    {id: 'ШД', email: 'i4', password: 'i4'}
];

module.exports = function DataModule (serviceLocator) {

    return {

        /**
         * Finds and returns user data by email and password
         * @param email
         * @param password
         * @param callback
         */
        authenticate: function (email, password, callback) {
            var investorId = investors.reduce(function (result, current) {
                if (current.email === email && current.password === password) {
                    result = current.id;
                }

                return result;
            }, null);

            if (!investorId) {
                return callback(null, false, {message: 'Пользователь с таким email и паролем не найден'});
            }

            // Успешно возвращаем id пользователя
            return callback(null, investorId);
        },

        /**
         * Returns user data
         * @param {string} userId
         * @param {function} callback
         */
        getUserData: function (userId, callback) {
            var data = cache.get(CACHE_KEY_PREFIX + userId);

            // Returns data immediately if cache is not empty
            if (data) {
                return process.nextTick(function () {
                    return callback(null, data);
                });
            }

            // Fetch data from google script
            return request({
                url: 'https://script.google.com/macros/s/AKfycbwPFl0Sd9HVhqvzHhgOGOz4GCyP673UhjVo43RhqbzBnXJtOAXO/exec?userId=' + encodeURIComponent(userId),
                method: 'GET',
                json: true,
                followAllRedirects: true
            }, function (err, response, body) {
                if (err) return callback(err);

                if (body && body.success) {
                    // Respond with data
                    callback(null, body.result);

                    // Store data into the cache
                    cache.set(CACHE_KEY_PREFIX + userId, body.result);
                    return;
                }

                console.log('Get data error: ', JSON.stringify({error: err, response: response, body: body}));

                err = new Error(body.message || 'Unable to retrieve data');
                err.status = body.status || 500;

                return callback(err);
            });
        },

        /**
         * Flushes cache data
         * @param {Function} cb - callback
         */
        flushCache: function (cb) {
            cache.del(investors.map(function (investor) {
                return CACHE_KEY_PREFIX + investor.id;
            }), function (err) {
                console.log('cache clear: ' + err);
            });

            return process.nextTick(function () {
                return cb(null);
            });
        }
    };
};
