/**
 * Module dependencies.
 */
var serviceLocator = require('../lib/serviceLocator')();

// REGISTER COMPONENTS ---------------------------------------------------------
// =============================================================================

// configs
serviceLocator.register('configCommon', require('../config/common'));

// middlewares
serviceLocator.factory('RequireLoginMiddleware', require('../middlewares/RequireLoginMiddleware'));

// modules
serviceLocator.factory('DataModule', require('../modules/DataModule'));

// Application
serviceLocator.factory('Application', require('../Application'));

// BOOTSTRAP APPLICATION -------------------------------------------------------
// =============================================================================

var Application = serviceLocator.get('Application');

var application = (new Application()).start();

// If the Node process ends, invoke graceful exit
process
    .on('SIGINT', application.gracefulExit.bind(this))
    .on('SIGTERM', application.gracefulExit.bind(this))
    .on('uncaughtException', application.gracefulExit.bind(this));