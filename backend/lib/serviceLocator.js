module.exports = function() {
    var dependencies = {}
        , factories = {}
        , serviceLocator = {};

    // Associates a component name against a factory
    serviceLocator.factory = function (name, factory) {
        factories[name] = factory;
    };

    // Associates a component name directly with an instance
    serviceLocator.register = function (name, instance) {
        dependencies[name] = instance;
    };

    // Retrieves a component by its name.  If an instance is already available, it simply returns it;
    // otherwise, it tries to invoke the registered factory to obtain a new instance
    serviceLocator.get = function (name) {
        if (!dependencies[name]) {
            var factory = factories[name];

            dependencies[name] = factory && factory(serviceLocator);
            if (!dependencies[name]) {
                throw new Error('Cannot find module: ' + name);
            }
        }

        return dependencies[name];
    };

    return serviceLocator;
};